# Projeto WebChat

Atividade prática necessária para o processo seletivo da SmarttBot. Esta atividade é composta por uma aplicação de chat, modularizada entre backend e frontend. O backend consiste em uma aplicação escrita em Go. Já o frontend, é uma aplicacão escrita em react.

Foi solicitada a criação de dois ambientes, desenvolvimento e produção. Para tal foi escolhido o Gitlab para hospedar o código e separar os dois ambientes em branches. Sendo o de desenvolvimento o branch chamado de **dev** e o branch de produção foi chamado de **master**.

link deste repositório: https://gitlab.com/lucasmaiasilva/project-x

Os ambientes foram criados utilizando imagens Docker. 

## CI/CD

A ferramenta de CI utilizada foi o Gitlab CI, assim são listadas abaixo as funcionalidades utilizadas para obter um melhor desempenho e qualidade do processo de CI/CD.

### Jobs e stages

O Gitlab CI assim como outras ferramentas de CI possui a segmentação das tarefas a serem executadas no pipeline em estágios. Os estágios, são uma maneira simples de executar os jobs em paralelo, desta forma, jobs que não possuem dependência entre si podem ser executados em paralelo para aumentar o desempenho.

Stages:
 - code-analysis (executado para todos os branches)
 - build-and-test (executado para todos os branches)
 - build-docker (executado apenas por master e dev - o push da imagem só é feito em master)
 - build-docker-compose (o build do docker compose é validado apenas em dev)

### Code Quality (Code climate integration)

O GitLab oferece a possibilidade de integração com a ferramenta de análise estática de código code climate. No caso foi implementado o job que realizar a análise do código, é gerado um artefato e o mesmo é disponibilizado para acesso depois. Existem formas melhores de integrações e mais visuais, permitindo que os resultados da análise de código fiquem exibidas nos Pull Requests ou Merge Requests. 

Outra funcionalidade que esta ferramenta possui é a cobertura de testes e a capacidade de configurar quality gates que podem bloquear o Merge Request. Desta forma, é possível de garantir um nível de cobertura de testes e qualidade de código de acordo com o desejado. Este passo também facilita muito o processo de code review, deixando o código mais limpo e elegante.

### Cache

A funcionalidade de cache otimiza o download de dependências, processo demorado, e agravado em sistemas que utilizam node. O node modules o maior vilão de qualquer npm install pode chegar a baixar uma quantidade enorme de dados e deixar mais lento todo o processo. Esta funcionalidade permite reutilizar dados baixados até entre branches. Caso o build falhe por causa do cache existe a possibilidade de limpar a cache.

### Branches

O meu projeto possui duas branches que são fixas sempre. A branch de master representando o ambiente de produção e a branch de dev que representa o ambiente de desenvolvimento. A única branch que realiza deploy, no caso o push das imagens para o docker hub é a de master. A branch de dev realiza o build dos programas, das imagens e ainda do docker compose para validar que tudo está ok.

As outras possibilidades de branch são as novas features, chamadas de feature branch. Correção de bugs chamadas de bug fix e por último as correções urgentes chamadas de hot fix. As branches de feature e bug são sempre criadas a partir de dev. A branch de hot fix é criada a partir de master para que seu merge seja feito instantaneamente e assim garantir celeridade ao processo de correção de bugs. As outras branches seguem o processo de merge request gradativo, como foram clonadas a partir dev, elas devem ser mescladas em dev e posteriormente dev em master. Sempre respeitando o processo de merge request executando o pipeline e code review.

Lista de nomes das branches

- master
- dev
- ft-branch
- bf-branch
- hf-branch

## Ambiente de Desenvolvimento
### Front

A imagem docker utilizada para realizar o build do frontend foi a: **node:12.2.0-alpine**. As imagens alpine possuem tamanho reduzido, impactando diretamente no pipeline e no deploy da aplicação. O tamanho total da imagem foi cerca de 166 MB. Imagens reduzidas também possuem desempenho melhor das aplicações em produção. A quantidade de processos carregados é menor reduzindo o overhead de processos do sistema operacional que não são necessários para a aplicação.

Melhorias que ainda podem ser aplicadas, as variáveis definidas na imagem estão hardcoded. É possível que elas sejam passadas por argumento pelo CI e assim dependendo da branch que estamos a imagem é construída com valores diferentes. Esta parametrização ainda favorece a necessidade de trocar os valores sem ter que gerar alterações no script do CI, apenas alterando as variáveis definidas nas secrets do Gitlab.

### Build and Tests
O job de build and tests constitui basicamente nos seguintes passos:

```
    - cd frontend
    - npm install --silent
    - npm install react-scripts@3.0.1 -g --silent
    - npm run build
    - npm test
```
Estes passos são o mesmo para produção já que não há diferenças no build de produção e também não há diferenças na instalação das dependências de produção. Desta forma poderiam se diferenciar e aumentar o desempenho do build.

### Build Docker

Abaixo a Dockerfile para a criação da imagem docker. Foi escolhida uma das versões mais novas de node, o código funcionou corretamente.

```
FROM node:12.2.0-alpine

ENV REACT_APP_BACKEND_WS ws://localhost:8080
ENV REACT_APP_BACKEND_URL http://localhost:8080

WORKDIR /app
COPY . .

RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent
RUN npm run build

CMD ["npm", "start"]
```

### Back

Assim como o front foi escolhida uma imagem alpine para realizar o build da imagem. Neste caso, a imagem do backend ficou menor que a do front, alcançando 148 MB. Os valores gasto pelas imagens impactam diretamente no custo, vários builds realizados por dia podem elevar os custos de storage. Sendo assim relevante possuir alguma rotina que limpe imagens mais antigas para evitar gastos desnecessários.

O código do backend não possui tests. Uma melhoria que poderia ser feita é a adição de testes para garantir um código de qualidade. O build do programa é feito de forma simples:

### Docker Image
Abaixo o dockerfile utilizado para realizar o build do backend. Uma melhoria que pode ser feita é a fixação da versão da imagem alpine utilizada para evitar quebras no código. A variável do endereço do REDIS utiliza o nome redis para a rede devido ao sistema de nomes utilizado pelo docker compose. O redis é acessado apenas internamente pelo backend. Já as outras integracões são acessadas por fora da rede do docker compose.

```
FROM golang:alpine

RUN apk update && apk add --no-cache git

WORKDIR /app
COPY . .

ENV ALLOWED_ORIGIN http://localhost:3000
ENV REDIS_ADDR redis:6379

RUN go get ./...
RUN go build .

CMD ["./backend"]
```


### Docker-Compose

Abaixo o arquivo utilizado pelo docker-compose. Ele integra as outras Dockerfiles e assim como elas utiliza uma imagem alpine. A porta do redis foi aberta apenas por necessidade de consultar se as mensagens estavam sendo inseridas no banco.

```
version: '3'
services:
  backend:
    build: backend/
    ports:
      - "8080:8080"
  frontend:
    build: frontend/
    ports: 
      - "3000:3000"
  redis:
    image: "redis:alpine"
    ports:
      - "6379:6379"
```


## Ambiente de produção
O ambiente de produção é representado pela branch de master e se difere principalmente pelo push das imagens ao docker hub. Ele é diferido pela seguinte linha de código:

```
    - if [ "$CI_COMMIT_REF_SLUG" == "master" ]; then docker push $IMAGE_TAG; fi
```

É utilizada uma variável interna do Gitlab para saber o branch que está recebendo o código e assim enviar a imagem para o docker hub. As tags utilizadas nas imagens os números dos jobs ids do gitlab ci.

Uma forma mais elegante de realizar o push das imagens seria um novo job só executado em master que utilizaria o estado salvo do job de build.

### Docker Hub

São utilizados os seguintes repositórios:

```
https://cloud.docker.com/repository/docker/lucasmaiasilva/project-x_backend/
https://cloud.docker.com/repository/docker/lucasmaiasilva/project-x_frontend/

```

## Melhorias

Dentro do que foi feito poderiam ser aplicadas as seguintes melhorias:

### Deploy
O deploy poderia ser realizado das aplicações em serviços na nuvem para execução de containers. O backend poderia ser hospedado no Amazon ECS ou no Google Cloud Run. O Redis poderia ser um serviço da AWS como o ElastiCache, a google também possui serviços semelhantes.

Já o front end poderia ser hospedado no S3 ou no cloud storage. Além disso, poderia ser hospedado também via CDN favorecendo a disponibilidade dos estáticos, facilidade para utilizar certificados HTTPS, e melhor tempo de resposta. 

### Terraform
Infraestrutura como código é uma tendência muito interessante, e junto desta demanda o Terraform (pelo menos em minha opinião) é um dos melhores para resolver este problema. Em arquiteturas mais complexas como da AWS que demandam a criação de VPCs, Subnets, Nat GWs, Internet GWs, Load Balancers, Tabelas de Roteamento, Elastic IPs, CloudFront Distributions, gerenciamento de nomes de domínio e entre outros, o terraform consegue deixar a criação e a gestão destes recursos mais simples. Além disso ele facilita a criação de tags para cada produto facilitando a gestão de custos.

### Monitoramento, Tracing e Profiling

Uma aplicação em produção necessita de todo um cuidado diferenciado. Visto que o cenário de desenvolvimento em sua maioria das vezes difere bastante do mundo real. A quantidade de carga e os diferentes perfis de usuários são significantemente relevantes para abordar pontos de falhas no software que não foram mapeados durante o processo de desenvolvimento. Por isso a necessidade de monitorar as aplicações. 

Atualmente contamos com ótimas ferramentas como o New Relic que consegue realizar um monitoramento bem fino da aplicação como um todo, sendo capaz de encontrar os pontos críticos da aplicação. Outra aplicação recente é da stack da google, o google APM, suíte do stackdriver que possui também a capacidade de depurar, rastrear e analisar a aplicação em produção.

### Testes (Cobertura de testes)

Por último, mas um dos mais importates, hoje em dia a infraestrutura vem sendo simplificada com as várias arquiteturas serverless que vem sendo disponibilizadas. Assim, abrindo espaço para focarmos no que realmente importa a qualidade do código. A qualidade do código é diretamente impactada pela cobertura de testes e pelos quality gates que podem ser configurados no analisador estático escolhido. Desta forma é possível saber as fragilidades do seu software e poder atacá-las. Garantindo assim um software mais seguro, estável e robusto.

